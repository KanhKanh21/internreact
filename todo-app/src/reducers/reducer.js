import { combineReducers } from "redux";
import { AppReducer } from "../App/App.reducer";
import { loginReducer } from "../pages/Login/Login.reducer";
import { todoListReducer } from "../pages/Todos/TodoList.reducer";

const rootReducer = combineReducers({
  app: AppReducer,
  login: loginReducer,
  todoList: todoListReducer,
});

export default rootReducer;
