// import * as types from "./App.constants";
import { LOGIN_SUCCESS } from "../pages/Login/Login.constant";
import produce from "immer";

const initialState = {
  isAuthenticated: false,
};

export const AppReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case LOGIN_SUCCESS:
        draft.isAuthenticated = true;
        break;
      default:
        return state;
    }
  });
