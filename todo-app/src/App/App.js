import React from "react";
import Routes from "../routes/routes";
import { ToastContainer } from "react-toastify";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div>
      <Routes />
      <ToastContainer />
    </div>
  );
}

export default App;
