import * as types from "./TodoList.constant";
import produce from "immer";

const initialState = {
  loading: false,
  todoList: [],
};

export const todoListReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case types.GET_TODO_LIST_REQUESTED:
        draft.loading = true;
        break;
      case types.GET_TODO_LIST_SUCCESS:
        draft.loading = false;
        draft.todoList = action.payload;
        break;
      case types.GET_TODO_LIST_FAILED:
        draft.loading = false;
        break;
      case types.ADD_TODO_SUCCESS:
        draft.loading = false;
        draft.todoList.push(action.payload);
        break;
      case types.DELETE_TODO_SUCCESS:
        draft.loading = false;
        draft.todoList = state.todoList.filter(
          (todo) => todo.id !== action.payload
        );
        break;
      case types.UPDATE_TODO_SUCCESS:
        draft.loading = false;
        const index = draft.todoList.findIndex(
          (todo) => todo.id === action.payload.id
        );
        draft.todoList[index] = action.payload;
        break;
      case types.TODO_REQUESTED:
        draft.loading = true;
        break;
      case types.TODO_FAILED:
        draft.loading = false;
        break;
      default:
        return { ...state };
    }
  });
