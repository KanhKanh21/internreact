import * as types from "./TodoList.constant";

export const getTodoListRequested = () => ({
  type: types.GET_TODO_LIST_REQUESTED,
});

export const getTodoListSuccess = (payload) => {
  return {
    type: types.GET_TODO_LIST_SUCCESS,
    payload,
  };
};

export const getTodoListFailed = (payload) => ({
  type: types.GET_TODO_LIST_FAILED,
  payload,
});

export const getTodoRequested = () => ({
  type: types.TODO_REQUESTED,
});

export const getTodoFailed = (payload) => ({
  type: types.TODO_FAILED,
  payload,
});

export const addTodo = (payload) => ({
  type: types.ADD_TODO_SUCCESS,
  payload,
});

export const updateTodo = (payload) => ({
  type: types.UPDATE_TODO_SUCCESS,
  payload,
});

export const deleteTodo = (payload) => ({
  type: types.DELETE_TODO_SUCCESS,
  payload,
});
