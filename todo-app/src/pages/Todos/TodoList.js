import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Popconfirm } from "antd";
import { MdDelete, AiTwotoneEdit } from "react-icons/all";
import ModalTodo from "../../components/ModalTodo/ModalTodo";
import * as actions from "../Todos/TodoList.thunk";
import "antd/dist/antd.css";
import "./TodoList.scss";

function TodoList() {
  const dispatch = useDispatch();
  const todoList = useSelector((state) => state.todoList.todoList);

  const [modalAdd, setModalAdd] = useState(false);
  const [isDelete, setIsDelete] = useState(null);
  const [modalEdit, setModalEdit] = useState(null);

  const onFinish = (todo) => {
    if (modalAdd) {
      dispatch(actions.addTodo(todo));
      setModalAdd(false);
    } else {
      dispatch(actions.editTodo(todo, modalEdit.id));
      setModalEdit(null);
    }
  };

  const handleDeleteTodo = () => {
    dispatch(actions.removeTodo(isDelete));
    setIsDelete(null);
  };

  useEffect(() => {
    dispatch(actions.getTodoList());
  }, []);

  return (
    <section className="container d-flex justify-content-center ">
      <div className=" bg-body rounded todo-form text-center">
        <h1 className="py-3 title-todos">TODOs APP</h1>
        <button
          className="btn btn-primary mb-3 border-0"
          onClick={() => {
            setModalAdd(true);
          }}
        >
          Add new task
        </button>
        <div className="d-flex justify-content-center task-form">
          <div>
            {todoList &&
              todoList.map((todo) => (
                <div
                  className="shadow my-4 bg-body rounded d-flex justify-content-between align-items-center todo-item"
                  key={todo.id}
                >
                  <div className="todo-main ">
                    <div className="p-2 task-name fw-bold">{todo.name}</div>
                  </div>
                  <div className="btn-group">
                    <AiTwotoneEdit
                      className="text-warning fs-2 mx-2 btn-edit"
                      onClick={() => {
                        setModalEdit(todo);
                      }}
                    />

                    <Popconfirm
                      title="Are you sure to delete this task?"
                      onConfirm={handleDeleteTodo}
                      onCancel={() => {
                        setIsDelete(null);
                      }}
                      okText="Yes"
                      cancelText="No"
                      placement="rightTop"
                    >
                      <MdDelete
                        className="text-danger fs-2 btn-delete"
                        onClick={() => {
                          setIsDelete(todo.id);
                        }}
                      />
                    </Popconfirm>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      <ModalTodo
        modalAdd={modalAdd}
        setModalAdd={setModalAdd}
        modalEdit={modalEdit}
        setModalEdit={setModalEdit}
        onFinish={onFinish}
      />
    </section>
  );
}

export default TodoList;
