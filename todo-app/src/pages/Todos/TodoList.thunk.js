import * as actions from "./TodoList.action";
import { toast } from "react-toastify";
import { getAllTodo, postTodo, deleteTodo, putTodo } from "../../api/todoApi";
import "react-toastify/dist/ReactToastify.css";

export const getTodoList = () => (dispatch) => {
  dispatch(actions.getTodoListRequested());
  return getAllTodo()
    .then((res) => {
      dispatch(actions.getTodoListSuccess(res));
    })
    .catch((err) => Promise.reject(dispatch(actions.getTodoListFailed(err))));
};

export const addTodo = (todo) => (dispatch) => {
  dispatch(actions.getTodoRequested());
  return postTodo(todo)
    .then((res) => {
      dispatch(actions.addTodo(res));
      toast.success("Add new task successfully");
    })
    .catch((err) => {
      Promise.reject(dispatch(actions.getTodoFailed(err)));
      toast.error("Add new task failed");
    });
};

export const removeTodo = (id) => (dispatch) => {
  dispatch(actions.getTodoRequested());
  return deleteTodo(id)
    .then((res) => {
      dispatch(actions.deleteTodo(id));
      toast.success("Delete task successfully");
    })
    .catch((err) => {
      Promise.reject(dispatch(actions.getTodoFailed(err)));
      toast.error("Delete task failed");
    });
};

export const editTodo = (todo, id) => (dispatch) => {
  dispatch(actions.getTodoRequested());
  return putTodo(todo, id)
    .then((res) => {
      dispatch(actions.updateTodo(res));
      toast.success("Edit task successfully");
    })
    .catch((err) => {
      Promise.reject(dispatch(actions.getTodoFailed(err)));
      toast.error("Edit task failed");
    });
};
