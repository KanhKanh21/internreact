export const GET_TODO_LIST_REQUESTED = "views/TodoList/GET_TODO_LIST_REQUESTED";
export const GET_TODO_LIST_SUCCESS = "views/TodoList/GET_TODO_LIST_SUCCESS";
export const GET_TODO_LIST_FAILED = "views/TodoList/GET_TODO_LIST_FAILED";

export const TODO_REQUESTED = "views/Todo/REQUESTED";
export const ADD_TODO_SUCCESS = "views/Todo/ADD_TODO_SUCCESS";
export const UPDATE_TODO_SUCCESS = "views/Todo/UPDATE_TODO_SUCCESS";
export const DELETE_TODO_SUCCESS = "views/Todo/DELETE_TODO_SUCCESS";
export const TODO_FAILED = "views/Todo/FAILED";
