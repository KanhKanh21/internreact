import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { loginApi } from "../../api/userApi";
import * as actions from "./Login.action";

export const login =
  ({ email, password }) =>
  (dispatch) => {
    dispatch(actions.loginRequested());
    return loginApi(email, password)
      .then((res) => {
        dispatch(actions.loginSuccess(res));
        toast.success("Logged in successfully");
      })
      .catch((err) => {
        Promise.reject(dispatch(actions.loginFailed(err)));
        toast.error("Login failed");
      });
  };
