import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { login } from "./Login.thunk";
import { PATH } from "../../constants/paths";
import "./Login.scss";

const mapDispatchToProps = {
  login,
};
const mapStateToProps = (state) => ({
  loading: state.loading,
});

const connector = connect(mapStateToProps, mapDispatchToProps);

const Login = (props) => {
  const { login, loading } = props;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const history = useHistory();

  function handleSubmit(e) {
    e.preventDefault();
    const message = [];
    if (!email) {
      message.email = "Please input your email";
    }
    if (!password) {
      message.password = "Please input your password";
    }
    setErrorMessage(message);
    if (Object.keys(message).length > 0) return false;
    if (!loading) {
      const payload = { email, password };
      login(payload).then((res) => {
        history.push(PATH.TODO);
      });
    }
    return true;
  }

  return (
    <div className="container d-flex justify-content-center">
      <div className=" bg-body rounded login-container">
        <header className="header text-center py-4">
          <h3>Login to your account</h3>
        </header>
        <form className="px-5 py-4 login-form" onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="email" className="form-label label">
              Email
            </label>
            <input
              type="email"
              id="email"
              className="form-control border-0 border-bottom py-2"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
            <p className="text-danger fst-italic text-msg my-1">
              {errorMessage.email}
            </p>
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label label">
              Password
            </label>
            <input
              type="password"
              id="password"
              className="form-control border-0 border-bottom py-2"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <p className="text-danger fst-italic text-msg my-1">
              {errorMessage.password}
            </p>
          </div>
          <div className="d-grid mt-5">
            <button type="submit" className="btn btn-primary">
              Login
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default connector(Login);
