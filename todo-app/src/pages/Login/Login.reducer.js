import * as types from "./Login.constant";
import produce from "immer";

const initialState = {
  id: null,
  loading: false,
};

export const loginReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case types.LOGIN_REQUESTED:
        draft.loading = true;
        break;
      case types.LOGIN_SUCCESS:
        draft.loading = false;
        draft.id = action.payload.id;
        sessionStorage.setItem("id", draft.id);
        break;
      case types.LOGIN_FAILED:
        draft.loading = false;
        break;
      default:
        return state;
    }
  });
