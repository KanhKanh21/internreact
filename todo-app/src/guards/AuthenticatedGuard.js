import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { PATH } from "../constants/paths";

function AuthenticatedGuard(props) {
  const { isAuthenticated, component: Component, ...rest } = props;
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isAuthenticated && !sessionStorage.getItem("id")) {
          return <Redirect to={PATH.LOGIN} />;
        }
        return <Component {...props} />;
      }}
    />
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.app.isAuthenticated,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticatedGuard);
