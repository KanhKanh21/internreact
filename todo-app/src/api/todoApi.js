import axiosClient from "../utilities/axiosClient";

export function getAllTodo() {
  const token = sessionStorage.getItem("id");
  return axiosClient.get(`/todos?access_token=${token}`);
}

export function postTodo(data) {
  const token = sessionStorage.getItem("id");
  return axiosClient.post(`/todos?access_token=${token}`, {
    name: data.name,
  });
}

export function deleteTodo(id) {
  const token = sessionStorage.getItem("id");
  return axiosClient.delete(`/todos/${id}?access_token=${token}`);
}

export function putTodo(data, id) {
  const token = sessionStorage.getItem("id");
  return axiosClient.put(`/todos/${id}?access_token=${token}`, {
    name: data.name,
  });
}
