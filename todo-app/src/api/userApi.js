import axiosClient from "../utilities/axiosClient";

export function loginApi(email, password) {
  return axiosClient.post(`/Accounts/login`, {
    email,
    password,
  });
}
