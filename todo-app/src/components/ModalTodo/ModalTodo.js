import React from "react";
import { Modal, Button, Form, Input } from "antd";
import "antd/dist/antd.css";
import "./ModalTodo.scss";

const ModalTodo = (props) => {
  const { modalAdd, setModalAdd, modalEdit, setModalEdit, onFinish } = props;
  return (
    <Modal
      centered
      visible={modalAdd || modalEdit}
      onCancel={() => {
        setModalAdd(false);
        setModalEdit(null);
      }}
      footer={[]}
      destroyOnClose={true}
    >
      <Form
        name="basic"
        labelCol={{ span: 5 }}
        wrapperCol={{ span: 17 }}
        initialValues={modalEdit || { name: "" }}
        onFinish={onFinish}
      >
        <h4 className="title-modal text-center mb-5">
          {modalEdit ? "EDIT TASK" : "ADD NEW TASK"}
        </h4>

        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: "Please enter the name!",
            },
          ]}
        >
          <Input className="inp-text" />
        </Form.Item>

        <div className="d-flex justify-content-center">
          <Button
            className="btn-submit btn mx-2"
            type="primary"
            htmlType="submit"
          >
            {modalEdit ? "Save" : "Submit"}
          </Button>
          <Button
            className="btn-cancel btn"
            onClick={() => {
              setModalAdd(false);
              setModalEdit(null);
            }}
          >
            Cancel
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default ModalTodo;
