import React from "react";
import { BrowserRouter } from "react-router-dom";
import TodoRoutes from "./TodoRoutes";
import LoginRoutes from "./LoginRoutes";

export default function Routes() {
  return (
    <BrowserRouter>
      <TodoRoutes />
      <LoginRoutes />
    </BrowserRouter>
  );
}
