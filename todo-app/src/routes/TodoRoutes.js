import React, { lazy, Suspense } from "react";
import { Switch } from "react-router-dom";
import AuthenticatedGuard from "../guards/AuthenticatedGuard";
import { PATH } from "../constants/paths";
import Loading from "../components/Loading/Loading";

const TodoList = lazy(() => import("../pages/Todos/TodoList"));

export default function TodoRoutes() {
  return (
    <Switch>
      <AuthenticatedGuard
        path={PATH.TODO}
        component={() => (
          <Suspense fallback={<Loading />}>
            <TodoList />
          </Suspense>
        )}
      />
    </Switch>
  );
}
